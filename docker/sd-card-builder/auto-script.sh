#!/usr/bin/env bash
cd ~
git clone https://gitlab.com/sean-burns-public/raspberry-pi-os/rpi23-gen-image.git
cd rpi23-gen-image
CONFIG_TEMPLATE=rpi3Pbuster ./rpi23-gen-image.sh
cd ~
cd rpi23-gen-image/images/buster
xz --keep *.img
cp *.img.xz /sd-card.img.xz
