# Raspberry Pi SD Card Builder

This repository provides an automated build system for producing binary image files for 'burning' onto SD cards for use within Raspberry Pi devices.

This build system is run from a Docker Container.

## Building the SD Card Image

The following instructions assume you are working from local copy of this repository.

1. Change the working directory to the path where the Dockerfile is located:

    ```bash
    cd <root-of-this-repository>
    cd docker/sd-card-builder
    ```

1. Use the Dockerfile in the current working directory to build a local Docker Image named `sd-card-builder-img`:

    ```bash
    docker build --tag sd-card-builder-img .
    ```

1. Start up a new Docker Container (named `sd-card-builder`) using the Docker Image that was just created (`sd-card-builder-img`) and run the script `/auto-script.sh` within the Container:

    ```bash
    docker run --cap-add=SYS_ADMIN --name sd-card-builder sd-card-builder-img /auto-script.sh
    ```

    Notes:

    - `--cap-add=SYS_ADMIN` is needed so that the `mount` command has the correct privileges (otherwise it will fail).

1. Copy the SD card image file from within the (now stopped) Docker Container to the local file system:

    ```cmd
    docker cp sd-card-builder:sd-card.img.xz ./
    ```

## Temporary Notes / Scratchpad

```bash
cd <root-of-this-repository>
cd docker/sd-card-builder
docker build --tag sd-card-builder-img:1 .
docker run --tty --interactive --detach --name sd-card-builder --cap-add=SYS_ADMIN sd-card-builder-img:1 /bin/bash
docker cp auto-script.sh sd-card-builder:/
docker exec sd-card-builder bash -c "chmod +x /auto-script.sh; /auto-script.sh"
exit
#docker exec -it sd-card-builder /bin/bash
#docker stop sd-card-builder
docker cp sd-card-builder:sd-card.img.xz ./

docker start -i sd-card-builder
# results in /build already exists, not proceeding.....so.....

# With container stopped:
docker commit sd-card-builder my_test_image
docker image ls
docker run -ti --name sd-card-builder2 --entrypoint=sh my_test_image
cd ~
cd rpi23-gen-image/images/buster
xz ./2020-08-23-arm-CURRENT-rpi3P-buster-armhf.img
cp 2020-08-23-arm-CURRENT-rpi3P-buster-armhf.img.xz /

# In command prompt:
docker cp sd-card-builder2:2020-08-23-arm-CURRENT-rpi3P-buster-armhf.img.xz ./

# To re-start sd-card-builder2 again after using it above:
docker start -i sd-card-builder2
```


## On a Ubuntu machine

### If Not Already Installed, Install Docker

These instructions are from here: https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository

#### Set Up The Docker Repository

1. Update the apt package index and install packages to allow apt to use a repository over HTTPS:

```bash
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
```

1. Add Docker’s official GPG key:

```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

1. Verify that you now have the key with the fingerprint `9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88`, by searching for the last 8 characters of the fingerprint.

```bash
sudo apt-key fingerprint 0EBFCD88
```

1. Use the following command to set up the stable repository.

```bash
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
```

#### Install Docker Engine

1. Update the apt package index, and install the latest version of Docker Engine and containerd:

```bash
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

1. Verify that Docker Engine is installed correctly by running the hello-world image:

```bash
sudo docker run hello-world
```


### If Not Already Installed, Install Git

1. Install Git:

```bash
sudo apt-get update
sudo apt-get install git
```

### Now do the Magic

1. Build the Docker Image for creating SD cards:

```bash
cd ~
git clone https://gitlab.com/sean-burns-public/raspberry-pi-os/docker-rpi23-gen-image.git
cd docker-rpi23-gen-image/docker/sd-card-builder
sudo docker build --tag sd-card-builder-img:1 .
```

1. TBD:

```bash
sudo docker run --tty --interactive --detach --name sd-card-builder --security-opt apparmor:unconfined --cap-add=SYS_ADMIN sd-card-builder-img:1 /bin/bash
sudo docker cp auto-script.sh sd-card-builder:/
sudo docker exec sd-card-builder bash -c "chmod +x /auto-script.sh; /auto-script.sh"
exit
#sudo docker exec -it sd-card-builder /bin/bash
#docker stop sd-card-builder
docker cp sd-card-builder:sd-card.img.xz ./
```

Notes:

- When Docker is running on a Linux host, `--security-opt apparmor:unconfined` is required in order to use the `mount` command.
